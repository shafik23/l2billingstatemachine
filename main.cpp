#include <L2BSM.h>
#include <QDebug>

int main()
{
    L2BSM state_machine(1.0, 0.1, 10.00, 120);

    state_machine.onJ1772Connected(true);
    return 0;
}
