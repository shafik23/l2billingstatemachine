#include <L2BSM.h>
#include <typeinfo>
#include <QDebug>
#include <QString>


struct L2BSM::Pimpl
{
    //////////////////////
    // These private inner classes define all the possible states in 
    // the state machine - there is a base class, then one subclass per
    // state.
    //////////////////////
    class State
    {
        public:
            // Called on the current_state whenever some external event occurs: 
            // E.g. time tick, J1772 disconnect, etc.
            // 
            // Each State is passed in the current "environment", which is really
            // just the Pimpl instance below.
            //
            // The return value is a pointer to a new state to transition to. To 
            // remain in the same state, return "this".
            virtual State* update(Pimpl* env) = 0;
            virtual ~State() {}

            virtual QString stringify()
            {
                return QString(typeid(*this).name());
            }
    };

    class StartState: public State
    {
        State* update(Pimpl* env)
        {
            // The start state is rather trivial: always transition
            // to the IGP state, regardless of what event occured.
            return env->IGP;
        }
    };

    class IGPState: public State
    {
        State* update(Pimpl* env)
        {
            (void)env;
            return this;
        }
    };

    class OccupyingState: public State
    {
        State* update(Pimpl* env)
        {
            (void)env;
            return this;
        }
    };

    class ChargingState: public State
    {
        State* update(Pimpl* env)
        {
            (void)env;
            return this;
        }
    };

    class FaultState: public State
    {
        State* update(Pimpl* env)
        {
            (void)env;
            return this;
        }
    };

    class EndState: public State
    {
        State* update(Pimpl* env)
        {
            (void)env;
            return this;
        }
    };

    //////////////////////

    Pimpl(qreal cost_per_charging_second, 
            qreal cost_per_parking_second, 
            qreal cost_per_kwh, 
            int igp);

    ~Pimpl();

    void updateStateMachine() 
    {
        auto next_state = current_state->update(this);
        qDebug() << "Updating state:" << current_state->stringify() << "->" << next_state->stringify();
        current_state = next_state;
    }

    // Session parameters
    qreal cost_per_charging_second;
    qreal cost_per_parking_second;
    qreal cost_per_kwh;
    int igp;

    // Charger live environment
    unsigned int seconds_elapsed;
    qreal kwh_elapsed;
    bool is_J1772_connected;
    bool is_J1772_charging;

    // Current State-Machine state
    State* current_state;

    // Instantiation of possible states
    State* START;
    State* IGP;
    State* OCCUPY;
    State* CHARGE;
    State* FAULT;
    State* END;
};

L2BSM::Pimpl::Pimpl(qreal cost_per_charging_second, 
                    qreal cost_per_parking_second, 
                    qreal cost_per_kwh, 
                    int igp) :
    cost_per_charging_second(cost_per_charging_second),
    cost_per_parking_second(cost_per_parking_second),
    cost_per_kwh(cost_per_kwh),
    igp(igp),
    seconds_elapsed(0),
    kwh_elapsed(0),
    is_J1772_connected(false),
    is_J1772_charging(false)
{
    // Instantiation of possible states
    START  = new StartState();
    IGP    = new IGPState();
    OCCUPY = new OccupyingState();
    CHARGE = new ChargingState();
    FAULT  = new FaultState();
    END    = new EndState();

    // Start off in the correct state
    current_state = START;
}

L2BSM::Pimpl::~Pimpl()
{
    delete START;
    delete IGP;
    delete OCCUPY;
    delete CHARGE;
    delete FAULT;
    delete END;
}


L2BSM::L2BSM(qreal cost_per_charging_second, qreal cost_per_parking_second, qreal cost_per_kwh, int igp) :
    pImpl(new Pimpl(cost_per_charging_second, cost_per_parking_second, cost_per_kwh, igp))
{
}

void L2BSM::onSecondTick()
{
    qDebug() << "onSecondTick()";
    pImpl->seconds_elapsed++;
    pImpl->updateStateMachine();
}

void L2BSM::onKwhUpdate(qreal kwh)
{
    qDebug() << "onKwhUpdate()";
    pImpl->kwh_elapsed = kwh;
    pImpl->updateStateMachine();
}

void L2BSM::onJ1772Connected(bool flag)
{
    qDebug() << "onJ1772Connected():" << flag;
    pImpl->is_J1772_connected = flag;
    pImpl->updateStateMachine();
}

void L2BSM::onJ1772Charging(bool flag)
{
    qDebug() << "onJ1772Charging():" << flag;
    pImpl->is_J1772_charging = flag;
    pImpl->updateStateMachine();
}

void L2BSM::onHardwareFault()
{
    qDebug() << "onHardwareFault():";
}

L2BSM::~L2BSM()
{
    delete pImpl;
}
