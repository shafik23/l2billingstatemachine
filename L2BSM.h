#pragma once
#include <QObject>


class L2BSM : public QObject
{
    Q_OBJECT

private:
    struct Pimpl;
    Pimpl* pImpl;

public:
    // All cost units are in dollars.
    // igp - Initial Grace Period, in seconds.
    L2BSM(qreal cost_per_charging_second,
          qreal cost_per_parking_second,
          qreal cost_per_kwh, 
          int igp);

    ~L2BSM();

public slots:
    void onSecondTick();
    void onKwhUpdate(qreal kwh);

    void onJ1772Charging(bool flag);
    void onJ1772Connected(bool flag);

    void onHardwareFault();
};
